package gumbyscout.resumeapp;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WorkexpListItem {
	public String dateRange;
	public String company;
	public String location;
	public String position;
	public Drawable image;
	public String locURI;
	
	public WorkexpListItem(){
		this.dateRange = "";
		this.company = "";
		this.location = "";
		this.position = "";
		this.locURI = "";
	}

	public WorkexpListItem(String dateRange, String company, String location, String postion, Drawable image, String locURI){
		this.dateRange = dateRange;
		this.company = company;
		this.location = location;
		this.position = postion;
		this.image = image;
		this.locURI = locURI;
	}

	public static class WorkexpListItemAdapter extends ArrayAdapter<WorkexpListItem>{
		private ArrayList<WorkexpListItem> items;
		private Context context;

		public WorkexpListItemAdapter(Context context, int textViewResourceId, ArrayList<WorkexpListItem> items){
			super(context, textViewResourceId, items);
			this.context = context;
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = LayoutInflater.from(context);
				v = vi.inflate(R.layout.workexp_list_item, null);
			}

			WorkexpListItem item = items.get(position);
			if (item != null) {
				//get the views
				TextView tvDateRange = (TextView) v.findViewById(R.id.workexp_list_item_daterange);
				tvDateRange.setText(item.dateRange);
				TextView tvPosition = (TextView) v.findViewById(R.id.workexp_list_item_position);
				tvPosition.setText(item.position);
				TextView tvCompany = (TextView) v.findViewById(R.id.workexp_list_item_company);
				tvCompany.setText(item.company);
				TextView tvLocation = (TextView) v.findViewById(R.id.workexp_list_item_location);
				tvLocation.setText(item.location);
				TextView tvLocURI = (TextView) v.findViewById(R.id.workexp_list_item_locURI);
				tvLocURI.setText(item.locURI);
				ImageView iv = (ImageView) v.findViewById(R.id.workexp_list_item_image);
				iv.setImageDrawable(item.image);
				
			}
			return v;
		}
	}
	
	public static class WorkexpListListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int postion,
				long arg3) {
			//get geoid
			final TextView tvLocURI = (TextView)view.findViewById(R.id.workexp_list_item_locURI);
			final Context context = arg0.getContext();
			
			AlertDialog.Builder builder = new AlertDialog.Builder(arg0.getContext());
	        builder.setMessage(R.string.workexp_diag_prompt)
	               .setPositiveButton(R.string.workexp_diag_confirm, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       //Launch google maps via intent
	                	   String locURI = (String) tvLocURI.getText();
	                	   Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
	                			   Uri.parse(locURI));
	                	   context.startActivity(intent);
	                   }
	               })
	               .setNegativeButton(R.string.workexp_diag_cancel, new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog
	                   }
	               });
	        builder.create();
	        builder.show();
			
		}
		
	}
	
}


