/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gumbyscout.resumeapp;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ListView;
import gumbyscout.resumeapp.WorkexpListItem.WorkexpListItemAdapter;
import gumbyscout.resumeapp.WorkexpListItem.WorkexpListListener;


public class MainActivity extends Activity {
    protected DrawerLayout mDrawerLayout;
    protected ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mSectionTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        mSectionTitles = getResources().getStringArray(R.array.sections_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mSectionTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
 
    }
    

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = new SectionFragment();
        Bundle args = new Bundle();
        args.putInt(SectionFragment.ARG_SECTION_NUMBER, position);
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mSectionTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Fragment that appears in the "content_frame"
     */
    public static class SectionFragment extends Fragment {
        public static final String ARG_SECTION_NUMBER = "section_number";
        private View rootView;

        public SectionFragment() {
            // Empty constructor required for fragment subclasses
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            int index = getArguments().getInt(ARG_SECTION_NUMBER);
            String section = getResources().getStringArray(R.array.sections_array)[index];
            rootView = null;
            
            
            //set the section fragment
            if(index == 0){
            	//biography section
            	rootView = inflater.inflate(R.layout.fragment_biography, container, false);
            	setHasOptionsMenu(true);
            	
            	//add and make bullet point rows
            	String randbullets [] = getResources().getStringArray(R.array.biography_array_randbullets);
            	TableLayout tl = (TableLayout) rootView.findViewById(R.id.biography_randBullets);
            	makeBullets(tl, randbullets, rootView.getContext());
            }
            else if(index == 1){
            	//education section
            	rootView = inflater.inflate(R.layout.fragment_education, container, false);
            	String courses[] = getResources().getStringArray(R.array.education_array_courses);
            	//add and make bullet point rows
            	TableLayout tl = (TableLayout)rootView.findViewById(R.id.education_courses_text);
            	makeBullets(tl, courses, rootView.getContext());
            }
            else if(index == 2){
            	//skills section
            	rootView = inflater.inflate(R.layout.fragment_skills, container, false);
            	//add and make bullet point rows
            	String programmingskills[] = getResources().getStringArray(R.array.skills_array_programming);
            	TableLayout programmingskillstl = (TableLayout)rootView.findViewById(R.id.skills_text_programming);
            	makeBullets(programmingskillstl, programmingskills, rootView.getContext());

            	String officeskills[] = getResources().getStringArray(R.array.skills_array_officeskills);
            	TableLayout officskillstl = (TableLayout)rootView.findViewById(R.id.skills_text_officeskills);
            	makeBullets(officskillstl, officeskills, rootView.getContext());
            	
            	String generalskills[] = getResources().getStringArray(R.array.skills_array_general);
            	TableLayout generalskillstl = (TableLayout)rootView.findViewById(R.id.skills_text_general);
            	makeBullets(generalskillstl, generalskills, rootView.getContext());
            }
            else if(index == 3){
            	//work experience section
            	rootView = inflater.inflate(R.layout.fragment_workexp, container, false);
            	
            	String [] workexp_array = getResources().getStringArray(R.array.workexp_array);
            	//convert string array to workexplistitem array
            	ArrayList<WorkexpListItem> itemArr = new ArrayList<WorkexpListItem>();
            	for(int i = 0; i < workexp_array.length; i+=5){
            		WorkexpListItem item = new WorkexpListItem();
            		item.dateRange = workexp_array[i];
            		item.company = workexp_array[i+1];
            		item.location = workexp_array[i+2];
            		item.position = workexp_array[i+3];
            		item.locURI = workexp_array[i+4];
            		itemArr.add(item);
            	}
            	
            	//set images
            	for(WorkexpListItem itm: itemArr){
            		if(itm.company.equals(getString(R.string.workexp_apsulibrary))){
            			itm.image = getResources().getDrawable(R.drawable.apsulibrary);
            		}
            		else if(itm.company.equals(getString(R.string.workexp_bentoncountylibrary))){
            			itm.image = getResources().getDrawable(R.drawable.bentoncountylibrary);
            		}
            		else if(itm.company.equals(getString(R.string.workexp_catfishplace))){
            			itm.image = getResources().getDrawable(R.drawable.catfishplace);
            		}
            		else if(itm.company.equals(getString(R.string.workexp_camden))){
            			itm.image = getResources().getDrawable(R.drawable.camden);
            		}
            		else if(itm.company.equals(getString(R.string.workexp_lyonlibrary))){
            			itm.image = getResources().getDrawable(R.drawable.lyonlibrary);
            		}
            	}
            	
            	//setadaptor for list view
            	ListView lv = (ListView)rootView.findViewById(R.id.workexp_list);
            	
            	lv.setAdapter(new WorkexpListItemAdapter(
            				rootView.getContext(), R.layout.workexp_list_item, itemArr));
            	lv.setOnItemClickListener(new WorkexpListListener());
            	
   
            }   
            
            getActivity().setTitle(section);
            return rootView;
        }
        
        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        	inflater.inflate(R.menu.biography_menu, menu);
        }
        
        
        public void makeBullets(TableLayout tl, String [] array, Context context){
        	//this goes through, making rows and adding them to the supplied table layout,
        	//making it into a bullet list with proper indentation
        	for(String str:array){
        		//make row
        		TableRow tr = new TableRow(context);
        		//make two text views for columns
        		TextView bullet = new TextView(context);
        		TextView item = new TextView(context);		
        		
        		//insert text
        		bullet.setText(R.string.bullet_point);
        		item.setText(str);
        		
        		//format them
        		bullet.setTextAppearance(context, android.R.style.TextAppearance_Large);
        		item.setTextAppearance(context, android.R.style.TextAppearance_Large);  
        		
        		//add layouts to row
        		tr.addView(bullet);
        		tr.addView(item);
        		
        		//add row to layout
        		tl.addView(tr);
        	}
        }
    }
    
    
    
}